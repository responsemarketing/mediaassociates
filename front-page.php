<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
<?php 

$images = get_field('home_page_slider');
$slide_count = 0;

if( $images ){ ?>
	
    <div class="slideshow home_slides">
    	<div class="slideset">
    		<ul class="slides">
	    		<?php foreach( $images as $image ){ ?>
	    			<?php $image_src = wp_get_attachment_image_src( $image['id'], 'home-slide' ); ?>
		    		<li class="slide-<?php echo $slide_count; ?>" style="background: url(<?php echo $image_src[0]; ?>);">
		    			
		    			<div class="holder">
		    				<div class="text-holder <?php echo get_field('text_position', $image['id'])." ". get_field('mobile_text', $image['id']);?>">
								
		    					<h2><?php echo $image['title']; ?></h2>
		    					<p><?php echo $image['caption']; ?></p>
		    				</div>
		    				<?php if($image['description'] != '') { ?>
		    					<span class="slide-info"><?php echo $image['description'] ?></span>
		    				<?php } ?>
		    			</div>
		    		</li>
		    		<?php $slide_count++; ?>
	    		<?php } //endforeach; ?>
	    	</ul>
	    </div>
    </div>
<?php } ?>
<nav class="add-nav">
	<?php
		wp_nav_menu (array(
			'theme_location' => 'services_menu',
			'menu_id' => 'menu',
			'container' => false,
			'container_class' => '',
			'menu_class' => '',
			'depth' => 1
		));
	?>

</nav><!-- / main-nav -->
<?php endwhile; ?>
<?php get_footer(); ?>